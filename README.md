#weixin-alert
很简单的步骤：

1. 登录web微信，打开开发者工具，切到http请求tab
2. 给要报警的人、或者群组发送一条微信
3. 在开发者工具中，copy出两条curl请求，一个是发送心跳的，一个是发送消息内容的。
4. 编译jar包，`mvn clean pakcage`
5. 将心跳curl保存成**weixin-heart.sh**,将发送curl保存成**weixin-send.sh**。
6. 将jar包和两个shell文件放在统一路径中，执行`java -jar weixinalert-1.0-SNAPSHOT-jar-with-dependencies.jar "Hello world!"`即可。

ps：心跳的那个请求，最好有个定时任务，crontab之类，一分钟一次，防止服务器踢掉该会话。缺点：不能退出手机上的微信（断网可以，只要不手动退出），不能再使用该微信号登录其他客户端的微信，如windows，mac，网页微信等，也就是说一个手机上的微信账号，最多额外产生一个sid，多了会踢掉之前的sid。